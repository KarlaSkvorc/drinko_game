package com.example.karla.drinko;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;


import java.util.concurrent.ThreadLocalRandom;
import java.util.Arrays;
import java.util.Random;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import static android.R.attr.button;

public class MainActivity_niko extends AppCompatActivity {



    public static final String EXTRA_NUMBER = "com.example.bus.example.EXTRA_NUMBER";

    private static ImageView imgview;
    private static android.widget.Button buttonsbm;
    private int current_img;
    private int previous_img=0;
    int[] images={R.drawable.i1, R.drawable.i2, R.drawable.i3, R.drawable.i4, R.drawable.i5, R.drawable.i6, R.drawable.i7, R.drawable.i8, R.drawable.i9, R.drawable.i10, R.drawable.i11, R.drawable.i12, R.drawable.i13, R.drawable.i14, R.drawable.i15, R.drawable.i16, R.drawable.i17, R.drawable.i18, R.drawable.i19, R.drawable.i20, R.drawable.i21, R.drawable.i22, R.drawable.i23, R.drawable.i24, R.drawable.i25, R.drawable.i26, R.drawable.i27, R.drawable.i28, R.drawable.i29, R.drawable.i30, R.drawable.i31, R.drawable.i32, R.drawable.i33, R.drawable.i34, R.drawable.i35, R.drawable.i36, R.drawable.i37, R.drawable.i38, R.drawable.i39, R.drawable.i40, R.drawable.i41, R.drawable.i42, R.drawable.i43, R.drawable.i44, R.drawable.i45, R.drawable.i46, R.drawable.i47, R.drawable.i48, R.drawable.i49, R.drawable.i50, R.drawable.i51, R.drawable.i52};
    Karta[] karte=new Karta[52];


    private static TextView text1;
    private static TextView text2;

    private static TextView text3;
    private static android.widget.Button button2;
    private static android.widget.Button button3;

    private static TextView text4;


    String v1;
    String v2;
    int vv1;
    int vv2;
    String vecjeManjse;
    int stP=0;

    int stevec=1;

    private static android.widget.Button button4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_niko);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Random rand = new Random();

        Karta k;
        for(int j=0;j<images.length;j++)
        {
            k=new Karta();
            k.slika=images[j];
            karte[j]=k;
        }

        int stevec = 1;

        for(int l=0;l<karte.length;l++)
        {
            karte[l].vrednost=stevec;
            stevec++;
            if(stevec==14)
            {
                stevec=1;
            }
        }


        for (int i = 0; i < karte.length; i++)
        {
            int randomIndexToSwap = rand.nextInt(karte.length);
            Karta temp = karte[randomIndexToSwap];
            karte[randomIndexToSwap] = karte[i];
            karte[i] = temp;
        }

        buttonClick();

        button2Click();

        button3Click();

        button4click();


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    public void buttonClick()
    {


        text1=(TextView)findViewById(R.id.textView1);
        text2=(TextView)findViewById(R.id.textView2);
        imgview=(ImageView)findViewById(R.id.imageView);
        buttonsbm=(Button)findViewById(R.id.button);
        text4=(TextView)findViewById(R.id.textView6);


        buttonsbm.setOnClickListener(
                new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        //current_img++;     //=ThreadLocalRandom.current().nextInt(0, 52 + 1);                    //int randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);
                        current_img=current_img % karte.length;
                        imgview.setImageResource(karte[current_img].slika);

                        text2.setText(String.valueOf(karte[current_img].vrednost));

                        if(current_img!=0)
                        {
                            previous_img=current_img-1;
                        }

                        current_img++;


                        text1.setText(String.valueOf(karte[previous_img].vrednost));

                        v1=text1.getText().toString();
                        v2=text2.getText().toString();
                        vecjeManjse=text3.getText().toString();

                        vv1=Integer.parseInt(v1);
                        vv2=Integer.parseInt(v2);

                        if(vecjeManjse=="NASLEDNJA KARTA VEČJA" && vv2<vv1)
                        {
                            stP++;
                            text4.setText(String.valueOf(stP));
                        }
                        else if(vecjeManjse=="NASLEDNJA KARTA MANJŠA" && vv2>vv1)
                        {
                            stP++;
                            text4.setText(String.valueOf(stP));
                        }


                        if(stevec==images.length+1)
                        {
                            imgview.setImageResource(R.drawable.cheers);

                            openActivity3();

                            //System.exit(0);
                        }
                        stevec++;
                    }
                }
        );
    }

    public void openActivity3()
    {
        int pozirki=stP;


        Intent intent = new Intent(this, Main3Activity_niko.class);  //////tutututut

        intent.putExtra(EXTRA_NUMBER,pozirki);

        startActivity(intent);
    }


    public void button2Click()
    {
        text3=(TextView)findViewById(R.id.textView5);
        button2=(Button)findViewById(R.id.button2);

        button2.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        text3.setText("NASLEDNJA KARTA VEČJA");
                    }
                }
        );
    }


    public void button3Click()
    {
        text3=(TextView)findViewById(R.id.textView5);
        button3=(Button)findViewById(R.id.button3);

        button3.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        text3.setText("NASLEDNJA KARTA MANJŠA");
                    }
                }
        );
    }

    public void button4click()
    {

        button4=(Button)findViewById(R.id.button4);

        button4.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openActivity2();
                    }
                }
        );
    }

    public void openActivity2()
    {
        Intent intent = new Intent(this, Main2Activity_niko.class);  //////


        startActivity(intent);
    }



}
