package com.example.karla.drinko;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class pomoc extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pomoc);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        globalni global = (globalni) getApplicationContext();

        if (global.getJezik()==1){
            TextView tv = (TextView)findViewById(R.id.textView4);
            tv.setText("Each player is given a number equal to one number on the board\n" +
                    "Each of the common fields represents one \"MiniGame\"\n" +
                    "MiniGames are: Bus, NAVIGA, Never Have i Ever and Hungarian (better known as FakJu)\n" +
                    "A player who lands on the field is required to complete a task assigned to him by MiniGame\n"+
                    "BAR-everybody drinks, STOP-you skip 2 throws, EX-drink all you have in your glass");

        }
        if (global.getJezik()==2){
            TextView tv = (TextView)findViewById(R.id.textView4);
            tv.setText("Vsak igralec dobi število katere je enako enemu številu na plošči\n" +
                    "Vsako skupno polje predstavlja eno \"MiniGame\"\n" +
                    "MiniGames so: Bus, NAVIGA, Nikoli nisem in madžarica (bolj znana kot FakJu)\n" +
                    "Igralec, ki pristane na igrišču, mora opraviti nalogo, ki mu jo je dodelil MiniGame\n"+
                    "BAR-vsi pijejo, STOP-preskačeš 2 meta, EX-popij sve kaj imaš v kozarcu");
        }
    }
}
