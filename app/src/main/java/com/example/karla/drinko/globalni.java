package com.example.karla.drinko;

import android.app.Application;

public class globalni extends Application {

    private int jezik;
    private int igralec_na_vrsti;

    public int getJezik() {
        return jezik;
    }

    public void setJezik(int jezik) {
        this.jezik = jezik;
    }

    public int getIgralec_na_vrsti() {
        return igralec_na_vrsti;
    }

    public void setIgralec_na_vrsti(int igralec_na_vrsti) {
        this.igralec_na_vrsti = igralec_na_vrsti;
    }
    public void setIgralec_na_vrsti(){
        this.igralec_na_vrsti = 0;
    }
}
