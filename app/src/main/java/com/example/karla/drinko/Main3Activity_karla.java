package com.example.karla.drinko;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Main3Activity_karla extends AppCompatActivity{
    private Button button;
    int index;
    int brojac=0;
    int virusi=0;
    int prije=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);//ZA BAR GORE
        setContentView(R.layout.activity_main3_karla);

        virusi=0;

        button = (Button) findViewById(R.id.button2_eng);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRules();
            }
        });
    }

    public void startAnimation() {
        Animation anima = AnimationUtils.loadAnimation(this, R.anim.karla_prelaz_tekst);
        TextView txt_animation = (TextView) findViewById(R.id.words_eng);
        txt_animation.startAnimation(anima);

    }

    public void openRules(){
        Intent intent = new Intent(this, pravila_karla.class);
        startActivity(intent);
    }

    public void generate_eng(View view) {//GENERIRANJE RANDOM BROJEVI
        brojac++;

        final ArrayList<String> arri = (ArrayList<String>) getIntent().getSerializableExtra("Imena");

        Random rand2 = new Random();
        int sz = rand2.nextInt(arri.size());
        String rand_name =arri.get(sz);

        if(brojac==20) {
            if(virusi==1){
                TextView words = (TextView) findViewById(R.id.words_eng);
                words.setText("VIRUS HAS ENDED\n"+"AND ALSO THE GAME!");
                words.setTextSize(50);

                TextView nasl = (TextView) findViewById(R.id.naziv_eng);
                nasl.setText(" ");

                ConstraintLayout lay = (ConstraintLayout) findViewById(R.id.layo3);
                lay.setBackgroundResource(R.drawable.nova_pozadina3);
                startAnimation();
            }
            else {
                TextView words = (TextView) findViewById(R.id.words_eng);
                words.setText("END!");
                words.setTextSize(50);
                startAnimation();

                TextView nasl = (TextView) findViewById(R.id.naziv_eng);
                nasl.setText(" ");

                ConstraintLayout lay = (ConstraintLayout) findViewById(R.id.layo3);
                lay.setBackgroundResource(R.drawable.nova_pozadina1);
            }

        }
        else if(brojac>20){
            Intent intent = new Intent(this, plosca.class);
            final ArrayList<String> array = (ArrayList<String>) getIntent().getSerializableExtra("Imena");
            intent.putExtra("Imena", array);
            startActivity(intent);
        }
        else {
            Random rand = new Random();
            int number=rand.nextInt(132);
            if(prije==1){
                while(number >= 51 && number < 81) number = rand.nextInt(132);
            }

            if (number >= 0 && number < 51) {//obicne
                List<String> list;
                list = new ArrayList<String>();
                list.add(rand_name + ", run around the room like a hen or drink 2 times.");
                list.add("If you've ever tried to use all four colors with four-color pen, drink 4 times. ");
                list.add("Give 2 sips if you've never watched any of the Lord of the Rings movies.");
                list.add("Everyone who has been to the toilet since the beginning of \"Drinko\" gives three sips.");
                list.add(rand_name + ", you give as much sips as you have uncles.");
                list.add(rand_name + ", you give as much sips as you have cousins.");
                list.add(rand_name + ", stand on your hands or drink 3 times.");
                list.add(rand_name + ", make a star or drink 3 times.");
                list.add(rand_name + ", make a reel forward or drink 2 times.");
                list.add(rand_name + ", give 4 sips to people taller than you. If that is impossible, drink 2 sips.");
                list.add("The person holding the phone must determine who drinks and how much.");
                list.add(rand_name + ", choose one item in the room. Whoever touches it first gives 5 sips.");
                list.add(rand_name + ", select one person in the room. Whoever touches it first gives 3 sips. That person gives sip to someone.");
                list.add(rand_name + ", mix \"Devil's drink\" to the person left of you. If you don’t want to, then drink 5 times, if that person does not want to drink, you both drink 10 times each.");
                list.add(rand_name + ", if you are in a relationship you give 4 sips, if you are not, you drink 3 sips.");
                list.add("The ones who has varnished someone's nails give 4 sips, the ones who has varnished the nails give 2 sips.");
                list.add("Girls, drink as much as your bra size (A = 1, B = 2,…).");
                list.add("Guys, drink as much as your panties size (S = 1, M = 2,…).");
                list.add("The first to remember an inspirational sentence gives three sips.");
                list.add(rand_name + ", dance Gangnam Style or drink 2 times.");
                list.add(rand_name + ", dance Robot Dance or drink 2 times.");
                list.add(rand_name + ", dance Macarena or drink 2 times.");
                list.add("Players who did not vote in the last election drink 4 sips.");
                list.add(rand_name + ", you give 4 sips to players who are thinner than you. If that is not possible, drink 2 times.");
                list.add(rand_name + ", you give 4 sips to players who are older than you. If that is not possible, you drink those 4 sips.");
                list.add(rand_name + ", you give 3 sips to players younger than you. If that is not possible, drink 5 sips.");
                list.add(rand_name + " if you stay quiet for 5 minutes, you can choose who will drink their whole drink.");
                list.add("Everyone who has had sexy dreams this week drinks 3 times.");
                list.add(rand_name + ", choose if you want to give 4 sips to the last player who was on the toilet or sip to everyone in the room.");
                list.add("Everyone younger than " + rand_name + ", drinks 3 times.");
                list.add("All older than " + rand_name + ", drink 3 times.");
                list.add(rand_name + ", give it as much sips as you wear white things.");
                list.add("All those who went to college with " + rand_name + ", drink 2 sips.");
                list.add("Everyone who went to high school with " + rand_name + ", drinks 3 sips.");
                list.add(rand_name + ", if you've ever solved a Rubik's Cube, you give 4 sips. If you've not, you drink those sips.");
                list.add(rand_name + ", make 5 pushups or drink 5 times.");
                list.add(rand_name + ", make 10 pushups or drink 5 times.");
                list.add("Drink sip if you are free, 2 if you are in a relationship, 3 if you have a friend with privileges and 4 if it is complicated.");
                list.add(rand_name + ", close your eyes. If you can tell exactly who drinks what, you give 5 sips. If you make a mistake, you drink 2 sips.");
                list.add("Everyone drinks as many times as they have years behind them since graduating from high school.");
                list.add("If you study, drink 2 times, if you work, drink 3 times. If neither, you drink 5 times.");
                list.add(rand_name + ", speak the English alphabet from behind to the beginning. If you make a mistake, you drink 4 times.");
                list.add(rand_name + ", if you would rather have a dog, you give 2 sips. If a cat, you give one sip.");
                list.add(rand_name + ", you drink as much sips as there are empty glasses on the table.");
                list.add("All who have a driver's license and actively use it drink 1, who do not have a driver's license 3 times, and who have and do not use it 5 times!");
                list.add("Drinks to the freshest driver at the table!");
                list.add("The one who have had driving licence the longest, gives 5 sips.");
                list.add(rand_name + ", drink without hands. If you succeed, you give 3 sips, if not, you give those sips a drink.");
                list.add(rand_name + " if you came on foot or with bike, drink 1 sip, if you came by public transport, you drink 3 sips.");
                list.add(rand_name + ", how everyone the last 5 pictures in your gallery. If you refuse, you drink 5 times.");
                list.add(rand_name + ", give your unlocked cell phone to the person right of you for one minute. If you refuse, you have to finish your drink.");



                Collections.shuffle(list, new Random(System.nanoTime()));
                TextView words = (TextView) findViewById(R.id.words_eng);
                words.setText(list.get(index++));
                words.setTextSize(20);
                prije=0;


                TextView nasl = (TextView) findViewById(R.id.naziv_eng);
                nasl.setText("REGULAR TASK");

                ConstraintLayout lay = (ConstraintLayout) findViewById(R.id.layo3);
                lay.setBackgroundResource(R.drawable.nova_pozadina1);
                startAnimation();

            }
            else if (number >= 51 && number < 81) {//virus
                if(virusi==0) {

                    virusi++;
                    List<String> list2;
                    list2 = new ArrayList<String>();
                    list2.add(rand_name + ", turn your back on everyone in the room. We'll tell you when it's time to turn back.");
                    list2.add(rand_name + ", go to the corner and think what you've just said. We'll tell you when it's time to go back.");
                    list2.add("It's time for a imagine man! Every time anyone drinks, get an imaginary man down from your drink, and when you're done, you have to put it back up. Who forgets or messes it up, has to drink 3 times.");
                    list2.add("Every boy drinks twice when a girl drinks.");
                    list2.add("Every girl drinks twice when a boyfriend drinks.");
                    list2.add("It's time for the pirates! Each player must say \"ARR\" at the beginning and end of the sentence. Whoever goes wrong drinks 2 sips.");
                    list2.add("It's time for \"John\"! Each time you have to say \"yes no\" at the beginning and end of a sentence. Whoever goes wrong drinks 2 sips.");
                    list2.add("Everyone in jeans drinks 2 sips every time someone drinks.");
                    list2.add("Everyone in short shirts drinks 2 sips each time someone drinks.");
                    list2.add("Everyone with black hair drinks sip every time a girl drinks.");
                    list2.add(rand_name + ", choose two people to swap seats. You drink 1 sip each time you do this.");
                    list2.add("It's time for a straw! Every time anyone drinks, take an imaginary straw out of your drink, and when you're done, you have to put it back inside. Remember to keep it in your hand at all times while drinking and remember to shake it to prevent spills.");
                    list2.add("It's time for the \"mourner\"! " + rand_name + ", you are a mourner, every time you laugh, you have to drink sip.");
                    list2.add("It is forbidden to touch hair and face! Whoever makes the mistake drinks 4 times.");
                    list2.add("No smoking! Anyone who wants to smoke must drink 5 times.");
                    list2.add("Do not look in the eyes! Whoever looks into each other's eyes drinks 3 times.");
                    list2.add("It's time for the Jen Selter Challenge! Each time someone wants to get out of the room, they have to do 10 squats. The one who forgets, drinks 2 times.");
                    list2.add("It's Pause Time! Have each player turn on the sound and internet on their cellphone and place their cellphone in the middle of the table. Each time someone is notified, he gives 3 sips.");
                    list2.add("Every time "+ rand_name + " stands up, their neighbors drink 5 times.");
                    list2.add("It's time for a tea party! Every time someone drinks, they have to be with their thumb raised. Whoever makes mistake, drinks 3 sips.");
                    list2.add("Let's get serious! No one should laugh. Who laughs, drinks 3 sips.");
                    list2.add(rand_name + ", get up and dance! If you stop, have a drink!");
                    list2.add(rand_name + ", you are a magician, abrakadabra! Swap someone's drink when you want.");
                    list2.add(rand_name + ", you are in charge! Every time you clap your hands, everyone in the room has to drink!");
                    list2.add("Each player must speak as if they have a mouth full of food. Whoever forgets drinks 3 times!");
                    list2.add(rand_name + ", every time someone completes a sentence you have to say \"obviously ...\". If you forget, have a drink!");
                    list2.add(rand_name + ", you've became a grandmother! You must not show your teeth to anyone. If you show, you drink 3 times.");
                    list2.add(rand_name + ", became a servant of the person left of you. You can fulfill up to 5 of his wishes.");
                    list2.add("New Rule! Every time someone speaks, they must complete the sentence \"" + rand_name + ", you are a jerk!\".");
                    list2.add("It's time for cowboys and Indians! The guys are cowboys and every time they have to start the sentence with \"Howdy!\" Girls are Indians and they have to yell “ooo” while covering their mouth with one hand while they talk.");


                    Collections.shuffle(list2, new Random(System.nanoTime()));
                    TextView words = (TextView) findViewById(R.id.words_eng);
                    words.setText(list2.get(index++));
                    words.setTextSize(20);


                    TextView nasl = (TextView) findViewById(R.id.naziv_eng);
                    nasl.setText("VIRUS");
                    prije=1;


                    ConstraintLayout lay = (ConstraintLayout) findViewById(R.id.layo3);
                    lay.setBackgroundResource(R.drawable.nova_pozadina3);
                    startAnimation();
                }
                else{
                    TextView words = (TextView) findViewById(R.id.words_eng);
                    words.setText("THE VIRUS HAS ENDED!");
                    words.setTextSize(50);

                    TextView nasl = (TextView) findViewById(R.id.naziv_eng);
                    nasl.setText(" ");


                    ConstraintLayout lay = (ConstraintLayout) findViewById(R.id.layo3);
                    lay.setBackgroundResource(R.drawable.nova_pozadina3);
                    startAnimation();
                    virusi=0;
                }

            }
            else{//
                List<String> list3;
                list3 = new ArrayList<String>();
                list3.add("Speak in a circle the accessories you can see at the dentist. Who knows no more, drinks 3 times. " + rand_name + ", you start.");
                list3.add("Speak in a circle, each has 2 seconds to say a word that is related to the word spoken by the user before. " + rand_name + ", you start. The one who no longer knows or mistakes, 3 times ago.");
                list3.add("Speak in a circle, \"How much wood would a woodchuck chuck if a woodchuck could chuck wood?\" the one who makes the mistake, drinks 3 times. " + rand_name + ", you start.");
                list3.add("Speak in a circle, \"How can a clam cram in a clean cream can?\" the one who makes the mistake drinks 2 times. " + rand_name + ", you start.");
                list3.add("Speak in a circle accessorier for cars. Who fails, drinks 3 times. " + rand_name + ", you start.");
                list3.add("Speak in a circle useful items from the high school that each user attended. " + rand_name + ", you start. Whoever runs out of ideas first drinks 2 times.");
                list3.add("Speak in a circle about Marvle villains. Whoever makes the first mistake or does not know, drinks 3 times. " + rand_name + ", you start.");
                list3.add("Speak in circle about X-men. Whoever makes the first mistake or does not know, drinks twice. " + rand_name + ", you start.");
                list3.add("Speak in a circle about programming languages. Who fails, drinks 4 times.");
                list3.add("Speak in a circle about fairy tale monsters. Whoever makes the first mistake or does not know, drinks 2 times.");
                list3.add("\"For or against hair Gel\"? All vote at the same time, the losing team drinks twice.");
                list3.add("\"For or against girl perfumes”? All vote at the same time, the losing team drinks twice.");
                list3.add("\"For or against sex in public\"? All vote at the same time, the losing team drinks twice.");
                list3.add("\"For or against short jeans on guys\"? All vote at the same time, the losing team drinks twice.");
                list3.add("For or against communism? All vote at the same time, the losing team drinks twice.");
                list3.add("Speak in the circle about things with holes. The first one to make a mistake or doesn't know is drinking 3 times.");
                list3.add("\"Rum or vodka\"? All vote at the same time, the losing team drinks twice.");
                list3.add("\"Beer or wine\"? All vote at the same time, the losing team drinks twice.");
                list3.add("\"White or red wine\"? All vote at the same time, the losing team drinks twice.");
                list3.add("\"Marvel or DC\"? All vote at the same time, the losing team drinks twice.");
                list3.add("\"Going to heaven or being reincarnated into a better life\"? All vote at the same time, the losing team drinks twice.");
                list3.add("Every player has to say a thing that " + rand_name + " has more than the person right of them. The person across of them starts. Those who do not know drink 2 times.");
                list3.add("Every player must say a thing that " + rand_name + " has less than the person across of ythem. The person left of them starts. If somebody makes a mistake, they have to drink 2 times.");
                list3.add("Every player has to say the thing that bothers him about " + rand_name + ". The person right of them starts. Those who do not know drinks 2 times.");
                list3.add("Hair to the knee or iroquois? They all vote at the same time, the losing team drinks twice.");
                list3.add("List all countries around Croatia. Whoever makes mistake or does not know drink 2 times. Person, on which list end, has to drink 4 times. " + rand_name + ", start.");
                list3.add("List all countries surrounding Slovenia. Whoever makes mistake or does not know drink 2 times. Person, on which list end, has to drink 4 times. " + rand_name + ", start.");
                list3.add("Speak to the countries around the United Kingdom. Whoever goes wrong or does not know drink 2 times. On which stops, he drinks 3 times.");
                list3.add("\"States in the European Union\"! " + rand_name + ", you start. Whoever makes a mistake or doesn't know, drinks 3 times. On which stops, he drinks 2 times.");
                list3.add(rand_name + ", say first name, last name and date of birth of each player. For each mistake you made, that person gives 5 sips.");
                list3.add("It's time for \"Jellyfish\"! Each player has his head resting on the edge of the table. At the end of the countdown, everyone looks into one person's eyes. People who are looking each other have to \"inhale\" their drink.");
                list3.add("It's time for vela! If you have bela cards with you, each player has to take one. The player with the highest card gives 4 sips, the player with the lowest card drinks 2 sips. If you do not have cards, each player drinks 5 times.");
                list3.add("It's time for bela! If you have bela cards with you, each player has to take one. The player with the highest card drinks 4 sips, the player with the lowest card gives 2 sips. If you do not have cards, each player drinks 3 times.");
                list3.add("Speak in circle the last movie you've watched. If someone else has watched the same movie, they both drink 3 times.");
                list3.add("Speak in circle the last series you've watched. If someone else has watched the same series, they both drink 3 times.");
                list3.add("List strategy video games. Whoever makes mistake or does not know, they drink 2 times. Person on which list stops has to drink 3 times.");
                list3.add("List games for PS2. Whoever makes mistake or does not know, drinks 2 times. Person on which list stops has to drink 3 times.");
                list3.add("\"Dogs or Cats\"? All vote at the same time, the losing team drinks twice.");
                list3.add("It's time to finish the sentence! Each player says one word and the next player must repeat all the words from before and add his or her own. Remember the meaning of the sentence! Who makes mistake, drinks 2 times.");
                list3.add("It's waterfall time! One player starts to drink his drink, then the player after him and so in a circle. When the first player stops drinking, so do all the other players in a circle, one after the other. A player who starts or stops too early, drinks 3 more sips. If the waterfall was successful, starter drinks 3 sips" + rand_name + ", start.");
                list3.add("It's time for the \"Category\"! " + rand_name + ", come up with a category and all the other players have to tell something out of it until someone makes a mistake or doesn't know. That one drinks 3 sips.");
                list3.add("It's time for hygiene! Everybody go wash their hands, whoever washes first gives 2 sips, who last, drinks 2 sips.");
                list3.add("Speak in the circle favorite food from " + rand_name + ". Who fails, drinks 3 times. The person left of them starts.");
                list3.add(rand_name + ", say \"In my suitcase is ..\" and say one thing. Each player must repeat all items and add their item. If someone mistakes, they drink 5 times.");
                list3.add("\"80s Hairstyles or 80s Clothes\"? All vote at the same time, the losing team drinks twice.");
                list3.add("It's time for the song! " + rand_name + ", start singing some song and point to the player who will continue! This goes on until someone makes a mistake or doesn't know the words. Everyone who sang drinks 2 sips.");
                list3.add("\"X-ray view or laser view\"? All vote at the same time, the losing team drinks twice.");
                list3.add("List well-known criminals. Whoever makes the first mistake or does not know, drinks 2 times.");
                list3.add("Decide who is the smartest person in the room. That person must drink 3 times.");
                list3.add("Decide who is the nicest person in the room. That person must drink 3 times.");
                list3.add("It's time for one \"BUM\"! Start counting from one. For every number containing 3 or divisible by 3 you must say \"boom\". Whoever makes mistakes, drinks 3 sips.");



                Collections.shuffle(list3, new Random(System.nanoTime()));
                TextView words = (TextView) findViewById(R.id.words_eng);
                words.setText(list3.get(index++));
                words.setTextSize(20);


                TextView nasl = (TextView) findViewById(R.id.naziv_eng);
                nasl.setText("GAME");
                prije=0;

                ConstraintLayout lay = (ConstraintLayout) findViewById(R.id.layo3);
                lay.setBackgroundResource(R.drawable.nova_pozadina2);
                startAnimation();
            }
        }

    }
}
