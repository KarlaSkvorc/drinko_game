package com.example.karla.drinko;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Main2Activity_niko extends AppCompatActivity {

    private static android.widget.Button button1;
    private static android.widget.Button button2;
    private static android.widget.Button button3;
    private static TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2_niko);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        button1Click();

        button2Click();

        button3Click();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    public void button1Click()
    {
        text=(TextView)findViewById(R.id.textView);
        button1=(Button)findViewById(R.id.button1);

        button1.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        text.setText("Igra Bus: Najprej pritisnite gumb naslednja karta, da se vam prikaže 1. karta, nato pritisnite zeleno ali rdečo puščico (zelena pomeni, da predvidevate da bo naslednja karta večja od trenutne rdeča pa da bo manjša). Nato ponovno pritisnite gumb za naslednjo karto, če ste ugotovili napačno morate naredit požirek izbrane pijače.");
                    }
                }
        );
    }


    public void button2Click()
    {
        text=(TextView)findViewById(R.id.textView);
        button1=(Button)findViewById(R.id.button2);

        button1.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        text.setText("Igra Bus: Prvo pritisnite gumb naslednja karta, da možete vidjeti 1. karto, nakon toga pritisnite zeleno ali crveno strijelo (zelena znači, da pretpostavljate da bo naslednja karta večja od trenutne crvena pa da bo manja). Posle toga opet pritisnite gumb za sledečo karto, ako ste ugotovili pogrešno morate odpiti gutljaj pijače.");
                    }
                }
        );
    }



    public void button3Click()
    {
        text=(TextView)findViewById(R.id.textView);
        button3=(Button)findViewById(R.id.button3);

        button3.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        text.setText("Gamemode Bus: First press the button naslednja karta so you can see first card. After that you can choose between red and green arrow (green means that you assume that value of next card is going to be greater than current). Press button Naslednja karta again if you assumed wrong you have to drink one sip of your drink ");
                    }
                }
        );
    }

}
