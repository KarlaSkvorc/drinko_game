package com.example.karla.drinko;

import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

public class izbira_jezika extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_izbira_jezika);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        Button buttonAng = (Button) findViewById(R.id.button1ang);

        final globalni global = (globalni) getApplicationContext();

        buttonAng.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View v){
                global.setJezik(1);
                otpriFormu();
            }


        });
        Button buttonSlo = (Button) findViewById(R.id.button2slo);
        buttonSlo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View v){
                global.setJezik(2);
                otpriFormu();
            }
        });
        Button buttonHrv= (Button) findViewById(R.id.buttonhr);
        buttonHrv.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View v){
                global.setJezik(3);
                otpriFormu();
            }


        });


    }
    public void otpriFormu(){
        Intent intent = new Intent(this,vnos_imena.class);
        startActivity(intent);
    }

}
