package com.example.karla.drinko;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class MainActivity_karla extends AppCompatActivity{
    private Button button;
    int index;
    int brojac=0;
    int virusi=0;
    int prije=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);//ZA BAR GORE
        setContentView(R.layout.activity_main_karla);

        virusi=0;

        button = (Button) findViewById(R.id.button2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRules();
            }
        });
    }

    public void startAnimation() {
        Animation anima = AnimationUtils.loadAnimation(this, R.anim.karla_prelaz_tekst);
        TextView txt_animation = (TextView) findViewById(R.id.words);
        txt_animation.startAnimation(anima);

    }

    public void openRules(){
        Intent intent = new Intent(this, pravila_karla.class);
        startActivity(intent);
    }

    public void generate(View view) {//GENERIRANJE RANDOM BROJEVI
        brojac++;

        final ArrayList<String> arri = (ArrayList<String>) getIntent().getSerializableExtra("Imena");

        Random rand2 = new Random();
        int sz = rand2.nextInt(arri.size());
        String rand_name =arri.get(sz);


        if(brojac==20) {
            if(virusi==1){
                TextView words = (TextView) findViewById(R.id.words);
                words.setText("EPIDEMIJA JE ZAVRŠILA\n"+"ISTO KAO I IGRA");
                words.setTextSize(50);


                TextView nasl = (TextView) findViewById(R.id.naziv);
                nasl.setText(" ");

                ConstraintLayout lay = (ConstraintLayout) findViewById(R.id.layo);
                lay.setBackgroundResource(R.drawable.nova_pozadina3);

                startAnimation();
            }
            else {
                TextView words = (TextView) findViewById(R.id.words);
                words.setText("KRAJ!");
                words.setTextSize(50);
                startAnimation();

                TextView nasl = (TextView) findViewById(R.id.naziv);
                nasl.setText(" ");

                ConstraintLayout lay = (ConstraintLayout) findViewById(R.id.layo);
                lay.setBackgroundResource(R.drawable.nova_pozadina1);

                startAnimation();

            }
        }
        else if(brojac>20){
            Intent intent = new Intent(this, plosca.class);
            final ArrayList<String> array = (ArrayList<String>) getIntent().getSerializableExtra("Imena");
            intent.putExtra("Imena", array);
            startActivity(intent);
        }
        else{
            Random rand = new Random();
            int number=rand.nextInt(132);
            if(prije==1){
                while(number >= 51 && number < 81) number = rand.nextInt(132);
            }

            if (number >= 0 && number < 51) {//obicne
                List<String> list;
                list = new ArrayList<String>();
                list.add(rand_name + ", bježi oko sobe kao kokoš ili pij 2 put.");
                list.add("Ako si ikad pokušavao upotrijebiti sve četiri boje kod pinkale s četiri boje, piješ 4 put.");
                list.add("Daj 2 guca ako nikad nisi gledao niti jedan film od „Gospodara prstenova“.");
                list.add("Svi koji su već bili na wcu od početka „Drinkija“, daju tri guca.");
                list.add(rand_name + ", daješ toliko guca koliko imaš ujaka.");
                list.add(rand_name + ", daješ tolio guca koliko imaš sestrični.");
                list.add(rand_name + ", napravi stoj na rukama ili pij 3 put.");
                list.add(rand_name + ",  napravi zvijezdu ili pij 3 put.");
                list.add(rand_name + ", napravi kolut naprijed ili pij 2 put.");
                list.add(rand_name + ", daj 4 guca osobama višim od tebe. Ako je to nemoguće, pij 2 guca.");
                list.add("Onaj koji drži mobitel mora odrediti tko će eksati svoje piće.");
                list.add(rand_name + ", odredi jedan predmet u sobi. Tko ga prvi dotakne, daje 5 guca.");
                list.add(rand_name + ", odredi jednu osobu u sobi. Tko je prvi dotakne, daje 3 guca. Ta osoba daje 1 guc nekome.");
                list.add(rand_name + ", izmiksaj „Đavolje piće“ nekome od igrača. Ako ne želiš, piješ 5 put, ako ta osoba ne želi popiti, oboje pijete po svaki 10 put.");
                list.add(rand_name + ", ako si u vezi daješ 4 guca, ako nisi daješ 6 guca.");
                list.add("Onaj tko je lakirao nekome nokte daje 4 guca, onaj kome je netko drugi lakirao nokte daje 2 guca.");
                list.add("Cure, pijete onoliko kolika je vaša veličina grudnjaka (A=1, B=2, …).");
                list.add("Dečki, pijete onoliko koliko je veličina vaših gaća (S=1, M=2, …).");
                list.add("Prvi tko se sjeti inspiracijske rečenice daje tri guca.");
                list.add(rand_name + ", pleši Gangnam Style ili piješ 2 put.");
                list.add(rand_name + ", pleši Robot Dance ili piješ 2 put.");
                list.add(rand_name + ", pleši Macarenu ili piješ 2 put.");
                list.add("Igrači koji nisu glasali na zadnjim izborima piju 4 guca.");
                list.add(rand_name + ", daješ 4 guca igračima koji su tanji od tebe. Ako to nije moguće, piješ 2 put.");
                list.add(rand_name + ", daješ 4 guca igračima koji su stariji od tebe. Ako to nije moguće, piješ ta 4 guca.");
                list.add(rand_name + ", daješ 3 guca igračima koji su mlađi od tebe. Ako to nije moguće, piješ 5 guci.");
                list.add(rand_name + ", ako ostaneš tiho 5 minuta, možeš odabrati tko će eksati svoje piće.");
                list.add("Svatko tko je imao seksi snove ovaj tjedan pije 3 puta.");
                list.add(rand_name + ", biraj dal želiš dati 4 guca zadnjem igraču koji je bio na wcu ili 1 guc svakome u sobi.");
                list.add("Svi mlađi od " + rand_name + ", piju 3 put.");
                list.add("Svi stariji od " +rand_name + ", piju 3 put.");
                list.add(rand_name + ", daj toliko guci koliko bijelih stvari nosiš.");
                list.add("Svi koji su išli na faks s "+ rand_name + " piju 2 guca.");
                list.add("Svi koji su išli u srednju školu s " +rand_name + " piju 3 guca.");
                list.add(rand_name + ", ako si ikad riješio rubikovu kocku, daješ 4 guca. Ako nisi, piješ te guce.");
                list.add(rand_name + ", napravi 5 sklekova ili pij 5 put.");
                list.add(rand_name + ", napravi 10 sklekova ili pij 5 put.");
                list.add("Popij 1 guc ako si slobodan/na, 2 ako si u vezi, 3 ako imaš prijatelja/icu s povlasticama i 4 ako je komplicirano.");
                list.add(rand_name + ", zatvori svoje oči. Ako možeš reći točno tko pije što, daješ 5 guca. Ako pogriješiš, piješ 2 guca.");
                list.add("Svi piju toliko puta koliko godina je prošlo od završetka njihove srednje škole.");
                list.add("Ako studiraš piješ 2 put, ako radiš piješ 3 put. Ako niti jedno, piješ 5 put.");
                list.add(rand_name + ",  govori englesku abecedu od iza prema početku. Ako pogriješiš, piješ 4 put.");
                list.add(rand_name + ", ako bi rađe imala psa, daješ 2 guca. Ako mačku, daješ 1 guc.");
                list.add(rand_name + ", piješ toliko guca koliko je praznih čaša na stolu.");
                list.add("Svi koji imaju vozačku dozvolu i aktivno ju koriste piju 1 guc, koji nemaju vozačku dozvolu 3 guca, a koji je imaju i ne koriste 5 guca!");
                list.add("Pije najsvježiji vozač za stolom!");
                list.add("Tko najduže ima vozačku dozvolu, daje 5 guca.");
                list.add(rand_name + ", popij 1 guc svojega pića bez ruku. Ako uspiješ, daješ 3 guca, ako ne, te guce popiješ ti.");
                list.add(rand_name + ", ako si došao pješke ili biciklom ne piješ, ako si došao javnim prijevozom piješ 3 guca.");
                list.add("Pokaži svima zadnjih 5 slika u svojoj galeriji. Ako odbiješ, piješ 5 puta.");
                list.add(rand_name + ", daj svoj otključan mobitel desnom od sebe na jednu minutu. Ako odbiješ, eksaš svoje piće.");

                Collections.shuffle(list, new Random(System.nanoTime()));
                TextView words = (TextView) findViewById(R.id.words);
                words.setText(list.get(index++));
                words.setTextSize(20);


                TextView nasl = (TextView) findViewById(R.id.naziv);
                nasl.setText("OBIČAN ZADATAK");

                ConstraintLayout lay = (ConstraintLayout) findViewById(R.id.layo);
                lay.setBackgroundResource(R.drawable.nova_pozadina1);

                prije=0;
                startAnimation();

            }

            else if (number >= 51 && number < 81) {//virus

                if(virusi==0){
                    virusi++;

                    List<String> list2;
                    list2 = new ArrayList<String>();
                    list2.add(rand_name + ", okreni leđa svima u sobi. Mi ćemo ti reći kad je vrijeme da se okreneš nazad.");
                    list2.add(rand_name + ", idi u kut i razmisli što si rekao. Mi ćemo ti reći kad je vrijeme da se vratiš.");
                    list2.add("Vrijeme je za „čopleka“! Svaki put kad bilo tko pije, uzmite imaginarnog čopleka dole s vašeg pića, a kad završite, morate ga staviti nazad gore.");
                    list2.add("Svaki dečko pije duplo kad pije neka cura.");
                    list2.add("Svaka cura pije duplo kad pije neki dečko.");
                    list2.add("Vrijeme je za „pirate“! Svaki igrač na početku I na kraju rečenice mora reći „ARR“. Tko pogriješi, pije 2 guca.");
                    list2.add("Vrijeme je za „Janeza“! Svaki put na početku i na kraju rečenice morate reći „ja ne“. Tko pogriješi, pije 2 guca.");
                    list2.add("Svi u trapericama piju 2 guca svaki put dok netko pije.");
                    list2.add("Svi u kratkim majicama piju 2 guca svaki put dok netko pije.");
                    list2.add("Svi s crnom kosom piju guc svaki put dok pije neka cura.");
                    list2.add(rand_name + ", biraš dvije osobe koje će zamijeniti mjesta. Svaki put dok to napraviš, piješ 1 guc.");
                    list2.add("Vrijeme je za „slamku“! Svaki put kad bilo tko pije, uzmite imaginarnu slamku van s vašeg pića, a kad završite, morate je staviti nazad unutar. Ne zaboravite je držati cijelo vrijeme u ruci dok pijete i ne zaboravite je protresti da ne bi došlo do razlijevanja.");
                    list2.add("Vrijeme je za „tugića“! " + rand_name + ", ti si tugić, svaki put dok se nasmiješ, moraš piti guc.");
                    list2.add("Zabranjeno diranje kose i lica! Tko pogriješi, pije 4 put za svaki put kad se dotaknuo/la.");
                    list2.add("Zabranjeno pušenje! Tko želi pušiti, mora piti 5 put.");
                    list2.add("Zabranjeno gledanje u oči! Tko se međusobno pogleda u oči, oboje pije 3 put.");
                    list2.add("Vrijeme je za „Jen Selter Izazov“! Svaki put dok se netko želi ustati iz mjesta, mora napraviti 10 čučnjeva. Onaj koji zaboravi, pije 2 put.");
                    list2.add("Vrijeme je za „Pauzu“! Svaki igrač neka uključi zvuk i internet na svojem mobitelu i neka stavi mobitel na sredinu stola. Svaki put dok netko dobije obavijest, daje 3 guca.");
                    list2.add("Svaki put dok nekome nešto slučajno padne, njegovi susjedi piju 5 put.");
                    list2.add("Vrijeme je za „čajanku“! Svaki put dok netko pije, taj mora piti s podignutim malim prstom. Tko pogriješi, pije 3 guca.");
                    list2.add("Uozbiljimo se! Nitko se ne smije nasmijati. Tko se nasmije, pije 3 guca.");
                    list2.add(rand_name + ", ustaj i pleši! Ako prestaneš, popij guc!");
                    list2.add(rand_name + ", mađioničar si ti, abrakadabra! Zamijeni nečije piće kad kod želiš.");
                    list2.add(rand_name + " je glavna/i. Svaki put dok pljesneš rukama, svi piju (uključujući i tebe)!");
                    list2.add("Svaki igrač mora govoriti kao da ima puna usta hrane. Tko zaboravi, pije 3 put!");
                    list2.add(rand_name + ", svaki put dok netko završi rečenicu moraš reći „očito…“. Ako zaboraviš, popij guc!");
                    list2.add(rand_name + ", postala si baka! Ne smiješ nikome pokazati svoje zube. Ako pokažeš, piješ 3 put.");
                    list2.add(rand_name + ", je postao/la sluga od prijatelja preko njega! Možeš ispuniti do 5 njegovih želji.");
                    list2.add("Novo pravilo! Svaki put dok netko govori, mora završiti rečenicu s " + rand_name + ", ti si mutec!“.");
                    list2.add("Vrijeme je za kauboje i indijance! Dečki su kauboji i svaki put moraju početi rečenicu s „Howdy!“, a cure su indijanci i svaku rečenicu moraju završiti s lupanjem po ustima i vikanjem „oooo“. ");


                    Collections.shuffle(list2, new Random(System.nanoTime()));
                    TextView words = (TextView) findViewById(R.id.words);
                    words.setText(list2.get(index++));
                    words.setTextSize(20);


                    TextView nasl = (TextView) findViewById(R.id.naziv);
                    nasl.setText("VIRUS");

                    ConstraintLayout lay = (ConstraintLayout) findViewById(R.id.layo);
                    lay.setBackgroundResource(R.drawable.nova_pozadina3);

                    prije=1;
                    startAnimation();

                }
                else{
                    TextView words = (TextView) findViewById(R.id.words);
                    words.setText("EPIDEMIJA JE ZAVRŠILA!");
                    words.setTextSize(50);


                    TextView nasl = (TextView) findViewById(R.id.naziv);
                    nasl.setText(" ");

                    ConstraintLayout lay = (ConstraintLayout) findViewById(R.id.layo);
                    lay.setBackgroundResource(R.drawable.nova_pozadina3);

                    startAnimation();
                    virusi=0;
                }
            }
            else{
                List<String> list3;
                list3 = new ArrayList<String>();
                list3.add("Govorite u krug pribor kojega možeš vidjeti kod zubara. Tko više ne zna, pije 3 put. " + rand_name + ", ti počni.");
                list3.add("Govorite u krug, svaki ima 2 sekunde da veli riječ koja je povezana s riječju koju je rekao igrač prije. " + rand_name + ", ti počni. Onaj koji više ne zna ili pogriješi, prije 3 put.");
                list3.add("Govorite u krug, „Na štriku se suši šareni šosić“, onaj koji pogriješi, pije 3 put. " + rand_name + ", ti počni.");
                list3.add("Govorite u krug, „Na vrh brda vrba mrda“, onaj koji pogriješi pije 2 put. " + rand_name + ", ti počni.");
                list3.add("Govorite u krug pribor kod automehaničara. Tko više ne zna, pije 3 put. " + rand_name + ", ti počni.");
                list3.add("Govorite u krug korisne predmete iz srednje škole koju je svaki korisnik pohađao. " + rand_name + ", ti počni. Tko prvi ostane bez ideje, pije 2 put.");
                list3.add("Govorite u krug Marvel junake iz filmova. Tko prvi pogriješi ili ne zna, pije 3 put. "+ rand_name + ", ti počni.");
                list3.add("Govorite X-men junake. Tko prvi pogiješi ili ne zna, pije 2 put. " + rand_name + ", ti počni.");
                list3.add("Govorite u kruh programske jezike. Onaj tko pogriješi ili ne zna, pije 4 put. "+ rand_name + ", ti počni.");
                list3.add("Govorite u krug čudovišta iz bajki. Tko prvi pogriješi ili ne zna, pije 2 put. "+ rand_name + ", ti počni.");
                list3.add("„Za ili protiv gela za kosu“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("„Za ili protiv parfema na curama“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("„Za ili protiv seksa u javnosti“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("„Za ili protiv tajci na dečkima“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("„Za ili protiv komunizma“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("Govorite u krug stvari s rupama. Prvi koji pogriješi ili ne zna, pije 3 put. "+ rand_name + ", ti počni.");
                list3.add("„Rum ili vodka“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("„Pivo ili vino“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("„Bijelo ili crno vino“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("„Marvel ili DC“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("„Ići u raj ili biti reinkarniran u bolji život“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("Svaki igrač mora reći stvar koju " + rand_name + " ima više od svog desnog prijatelja. Osoba lijevo od " + rand_name + " počne. Tko ne zna ili pogriješi, pije 2 put.");
                list3.add("Svaki igrač mora reći stvar koju " + rand_name + " ima manje od svog lijevog susjeda. Osoba preko " + rand_name + " počne. Tko ne zna ili pogriješi, pije 3 put.");
                list3.add("Svaki igrač mora reći stvar koja mu smeta kod " + rand_name + ". Osoba desno od "+ rand_name + " počne. Tko ne zna ili pogriješi, pije 2 put.");
                list3.add("„Kosa do koljena ili irokeza“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("Govorite države koje okružuju Hrvatsku. Tko pogriješi ili ne zna pije 2 put. Na kome nabrojavanje stane, pije 3 put.");
                list3.add("Govorite države koje okružuju Sloveniju. Tko pogriješi ili ne zna pije 2 put. Na kome nabrojavanje stane, pije 3 put.");
                list3.add("Govorite države koje okružuju Ujedinjeno Kraljevstvo. Tko pogriješi ili ne zna pije 2 put. Na kome nabrojavanje stane, pije 3 put.");
                list3.add("„Države u europskoj uniji“! " + rand_name + ", ti počni. Tko pogriješi ili ne zna, pije 3 put. Na kome nabrojavanje stane, pije 2 put.");
                list3.add(rand_name + ", reci ime, prezime i datum rođenja od svakog igrača. Za koju osobu pogriješiš, ta osoba daje 2 guca.");
                list3.add("Vrijeme je za „Meduzu“! Svaki igrač ima glavu naslonjenu na rub stola. Na kraj odbrojavanja, svatko pogleda u oči jedne osobe. Osobe koje se gledaju u oči, eksaju svoje piće.");
                list3.add("Vrijeme je za jednu belu! Ako imate karte za belu kod sebe, neka svaki igrač uzme jednu. Igrač s najvećom kartom daje 4 guca, igrač s najmanjom pije 2 guca. Ako nemate karte, svaki igrač pije 5 put.");
                list3.add("Vrijeme je za jednu belu! Ako imate karte za belu kod sebe, neka svaki igrač uzme jednu. Igrač s najvećom kartom pije 4 guca, igrač s najmanjom daje 2 guca. Ako nemate karte, svaki igrač pije 3 put.");
                list3.add("Govorite u krug zadnje filmove koje ste pogledali. Ako su dvije osobe pogledale isti film, oboje piju 3 put. "+ rand_name + ", ti počni.");
                list3.add("Govorite u krug zadnju seriju koju ste gledali. Ako je netko još pogledao/la istu seriju, oboje piju 3 put. "+ rand_name + ", ti počni.");
                list3.add("Govorite strategijske video igrice. Tko pogriješi ili ne zna pije 2 put. Na kome stane, pije 3 put. "+ rand_name + ", ti počni.");
                list3.add("Govorite igrice za PS2. Tko pogriješi ili ne zna pije 2 put. Na kome stane, pije 3 put. "+ rand_name + ", ti počni.");
                list3.add("„Psi ili mačke“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("Vrijeme je za „Završi rečenicu“! Svaki igrač veli jednu riječ i idući igrač mora ponoviti sve riječi od prije i dodati svoju. Ne zaboravite na smislenost rečenice! Tko pogriješi, prije 2 put.");
                list3.add("Vrijeme je za „Slap“! Jedan igrač počinje piti svoje piće, zatim igrač poslije njega i tako u krug. Kada prvi igrač prestane piti, isto tako moraju i svi ostali igrači u krug jedan za drugim. Igrač koji prerano počne ili prestane, pije još 3 guca. Ako je slap prošao uspješno, prvi pije 3 guca. " + rand_name + ", počni.");
                list3.add("Vrijeme je za „Kategoriju“! " + rand_name + ", smisli neku kategoriju i svi ostali igrači moraju nabrojavati tako dugo dok neko ne pogriješi ili ne zna. Taj pije 3 guca.");
                list3.add("Vrijeme je za higijenu! Svi idite oprati ruke, tko prvi opere, daje 2 guca, tko zadnji, pije 2 guca.");
                list3.add("Govorite u krug najdraža jela od " + rand_name + ". Tko više ne zna ili pogriješi, pije 3 put. Desni prijatelj počinje.");
                list3.add(rand_name + ", velis „U moje kuferu se nalazi..“ i reci jedna predmet. Svaki igrač mora ponoviti sve predmete izgovorene prije njega i dodati neki svoj predmet. Tko pogriješi, pije 3 guca.");
                list3.add("„Frizura iz 80-ih ili odjeća iz 80-ih“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("Vrijeme je za pjesmu“ " + rand_name + ", počni pjevati neku pjesmu i pokaži na igrača koji će nastaviti! To se nastavlja sve dok netko ne pogriješi ili ne zna riječi. Svatko tko je pjevao pije 2 guca.");
                list3.add("„X-ray pogled ili laser pogled“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("Govorite u krug poznate kriminalce. Tko prvi pogriješi ili ne zna, pije 2 put. "+ rand_name + ", ti počni.");
                list3.add("Odlučite tko je najpametnija osoba u sobi. Ta osoba mora piti 3 put.");
                list3.add("Odlučite tko je najljepša osoba u sobi. Ta osoba mora piti 3 put.");
                list3.add("Vrijeme je za jedan „BUM“! Brojite od 1, na svaki broj koji sadrži 3 ili je djeljiv s 3 morate reći „bum“. Tko pogriješi, pije 3 guca.");


                Collections.shuffle(list3, new Random(System.nanoTime()));
                TextView words = (TextView) findViewById(R.id.words);
                words.setText(list3.get(index++));
                words.setTextSize(20);


                TextView nasl = (TextView) findViewById(R.id.naziv);
                nasl.setText("IGRA");

                ConstraintLayout lay = (ConstraintLayout) findViewById(R.id.layo);
                lay.setBackgroundResource(R.drawable.nova_pozadina2);
                prije=0;
                startAnimation();
            }
        }
    }
}
