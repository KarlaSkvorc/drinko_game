package com.example.karla.drinko;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class splash_karla extends AppCompatActivity {
    private TextView tw;
    private ImageView iw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);//ZA BAR GORE
        setContentView(R.layout.activity_splash_karla);
        iw = (ImageView)findViewById(R.id.logo);
        Animation moj_amin = AnimationUtils.loadAnimation(this, R.anim.karla_prelaz);
        iw.startAnimation(moj_amin);

        globalni global = (globalni) getApplicationContext();

        if(global.getJezik()==1){
            final ArrayList<String> arri = (ArrayList<String>) getIntent().getSerializableExtra("Imena");

            final Intent intent = new Intent(this,Main3Activity_karla.class);
            intent.putExtra("Imena", arri);

            Thread timer  = new Thread(){
                public void run (){
                    try {
                        sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    finally {
                        startActivity(intent);
                        finish();
                    }
                }
            };
            timer.start();


        }
        else if(global.getJezik()==2){
            final ArrayList<String> arri = (ArrayList<String>) getIntent().getSerializableExtra("Imena");

            final Intent intent = new Intent(this,Main2Activity_karla.class);
            intent.putExtra("Imena", arri);

            Thread timer  = new Thread(){
                public void run (){
                    try {
                        sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    finally {
                        startActivity(intent);
                        finish();
                    }
                }
            };
            timer.start();
        }
        else if(global.getJezik()==3){
            final ArrayList<String> arri = (ArrayList<String>) getIntent().getSerializableExtra("Imena");

            final Intent intent = new Intent(this,MainActivity_karla.class);
            intent.putExtra("Imena", arri);

            Thread timer  = new Thread(){
                public void run (){
                    try {
                        sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    finally {
                        startActivity(intent);
                        finish();
                    }
                }
            };
            timer.start();
        }
        else {
            final ArrayList<String> arri = (ArrayList<String>) getIntent().getSerializableExtra("Imena");

            final Intent intent = new Intent(this,Main3Activity_karla.class);
            intent.putExtra("Imena", arri);

            Thread timer  = new Thread(){
                public void run (){
                    try {
                        sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    finally {
                        startActivity(intent);
                        finish();
                    }
                }
            };
            timer.start();
        }
    }
}
