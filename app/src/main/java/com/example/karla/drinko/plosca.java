package com.example.karla.drinko;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.content.Intent;
import android.graphics.Matrix;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import java.util.Random;

public class plosca extends AppCompatActivity {
    Button generiraj_st;
    TextView tv_rand_st;
    Random r;
    int brojac=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_plosca);

        final globalni global = (globalni) getApplicationContext();


        if (global.getJezik()==1){
            TextView tv=(TextView) findViewById(R.id.textViewIme);
            tv.setText("Player name");
            TextView st=(TextView)findViewById(R.id.rand_stevilo);
            st.setText("Field number");

        }
        if (global.getJezik()==3){

            TextView tv1=(TextView) findViewById(R.id.textViewIme);
            tv1.setText("Ime igrača");
            TextView st=(TextView)findViewById(R.id.rand_stevilo);
            st.setText("Broj polja");
        }
        Button buttonH= (Button) findViewById(R.id.buttonHelp);
        buttonH.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View v){
                otpriFormu();
            }
        });

        final ArrayList<String> arr = (ArrayList<String>) getIntent().getSerializableExtra("Imena");


        r=new Random();
        generiraj_st=(Button) findViewById(R.id.buttonGen);
        tv_rand_st=(TextView)findViewById(R.id.rand_stevilo);
        final TextView tv= (TextView)findViewById(R.id.textViewIme);
        tv.setText(arr.get(global.getIgralec_na_vrsti()));

        generiraj_st.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int output=r.nextInt((31-1)+1)+1;
                tv_rand_st.setText("" + output);
                tv.setText(arr.get(global.getIgralec_na_vrsti()));
                global.setIgralec_na_vrsti(global.getIgralec_na_vrsti()+1);
                if (global.getIgralec_na_vrsti()>=arr.size())
                    global.setIgralec_na_vrsti();
            }
        });


        ImageView karla1=(ImageView)findViewById(R.id.imageViewK1);
        karla1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_karla();
            }
        });
        ImageView karla2=(ImageView)findViewById(R.id.imageViewK2);
        karla2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_karla();
            }
        });
        ImageView karla3=(ImageView)findViewById(R.id.imageViewK3);
        karla3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_karla();
            }
        });
        ImageView karla4=(ImageView)findViewById(R.id.imageViewK5);
        karla4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_karla();
            }
        });
        ImageView karla5=(ImageView)findViewById(R.id.imageViewK6);
        karla5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_karla();
            }
        });
        ImageView karla6=(ImageView)findViewById(R.id.imageViewK7);
        karla6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_karla();
            }
        });
        ImageView karla7=(ImageView)findViewById(R.id.imageViewK8);
        karla7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_karla();
            }
        });
        ImageView niko1=(ImageView)findViewById(R.id.imageViewN1);
        niko1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_niko();
            }
        });
        ImageView niko2=(ImageView)findViewById(R.id.imageViewN2);
        niko2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_niko();
            }
        });
        ImageView niko3=(ImageView)findViewById(R.id.imageViewN3);
        niko3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_niko();
            }
        });
        ImageView niko4=(ImageView)findViewById(R.id.imageViewN5);
        niko4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_niko();
            }
        });
        ImageView niko5=(ImageView)findViewById(R.id.imageViewN6);
        niko5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_niko();
            }
        });
        ImageView niko6=(ImageView)findViewById(R.id.imageViewN6);
        niko6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_niko();
            }
        });
        ImageView niko7=(ImageView)findViewById(R.id.imageViewN7);
        niko7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_niko();
            }
        });
        ImageView leon1=(ImageView)findViewById(R.id.imageViewL1);
        leon1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_leon();
            }
        });
        ImageView leon2=(ImageView)findViewById(R.id.imageViewL2);
        leon2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_leon();
            }
        });
        ImageView leon3=(ImageView)findViewById(R.id.imageViewL3);
        leon3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_leon();
            }
        });
        ImageView leon4=(ImageView)findViewById(R.id.imageViewL4);
        leon4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_leon();
            }
        });
        ImageView leon5=(ImageView)findViewById(R.id.imageViewL5);
        leon5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_leon();
            }
        });
        ImageView leon6=(ImageView)findViewById(R.id.imageViewL6);
        leon6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_leon();
            }
        });
        ImageView leon7=(ImageView)findViewById(R.id.imageViewL7);
        leon7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_leon();
            }
        });
        ImageView iva1=(ImageView)findViewById(R.id.imageViewI1);
        iva1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_iva();
            }
        });
        ImageView iva2=(ImageView)findViewById(R.id.imageViewI2);
        iva2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_iva();
            }
        });
        ImageView iva3=(ImageView)findViewById(R.id.imageViewI4);
        iva3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_iva();
            }
        });
        ImageView iva4=(ImageView)findViewById(R.id.imageViewI5);
        iva4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_iva();
            }
        });
        ImageView iva5=(ImageView)findViewById(R.id.imageViewI6);
        iva5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_iva();
            }
        });
        ImageView iva6=(ImageView)findViewById(R.id.imageViewI7);
        iva6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_iva();
            }
        });
        ImageView iva7=(ImageView)findViewById(R.id.imageViewI8);
        iva7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpriGamemode_iva();
            }
        });

    }

    public void otpriFormu(){
        Intent intent = new Intent(this,pomoc.class);
        startActivity(intent);
    }
    public void otpriGamemode_karla(){
        final ArrayList<String> arri = (ArrayList<String>) getIntent().getSerializableExtra("Imena");

        Intent intent = new Intent(this,splash_karla.class);
        intent.putExtra("Imena", arri);
        startActivity(intent);
    }
    public void otpriGamemode_niko(){
        Intent intent = new Intent(this, MainActivity_niko.class);


        startActivity(intent);
    }
    public void otpriGamemode_leon(){

    }
    public void otpriGamemode_iva(){

    }

}