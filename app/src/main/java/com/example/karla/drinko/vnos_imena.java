package com.example.karla.drinko;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;

public class vnos_imena extends AppCompatActivity {


    ListView display;
    ArrayList<String> Imena;
    Button potrdi;
    EditText ime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vnos_imena);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        final globalni global = (globalni) getApplicationContext();



        if (global.getJezik()==1){
            EditText imeIgralca=(EditText)findViewById(R.id.ime);
            imeIgralca.setText("Name");

        }
        if (global.getJezik()==3){
            EditText imeIgralca=(EditText)findViewById(R.id.ime);
            imeIgralca.setText("Ime");

        }
        Imena=new ArrayList<>();
        ime=(EditText)findViewById(R.id.ime);
        potrdi=(Button)findViewById(R.id.buttonVnos2);
        display =(ListView)findViewById(R.id.lv);

        potrdi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String value = ime.getText().toString();
                if (Imena.contains(value)) {
                    if (global.getJezik() == 1) {
                        Toast.makeText(getBaseContext(), "Name already exists", Toast.LENGTH_LONG).show();
                    } else if (global.getJezik() == 2) {
                        Toast.makeText(getBaseContext(), "Ime ze obstaja", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getBaseContext(), "Ime vec postoji", Toast.LENGTH_LONG).show();
                    }
                } else if (value == null || value.trim().equals("")) {
                    if (global.getJezik() == 1) {
                        Toast.makeText(getBaseContext(), "Name fied empty", Toast.LENGTH_LONG).show();
                    } else if (global.getJezik() == 2) {
                        Toast.makeText(getBaseContext(), "Ime ni vpisano", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getBaseContext(), "Polje s imenom prazno", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Imena.add(value);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(vnos_imena.this, android.R.layout.simple_list_item_1, Imena);
                    display.setAdapter(adapter);
                    ime.setText("");
                }


            }
        });

        Button buttonPokreni = (Button) findViewById(R.id.buttonKonec);
        buttonPokreni.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View v){
                otpriFormu();
            }


        });
    }
    public void otpriFormu(){
        Intent intent = new Intent(this,plosca.class);
        intent.putExtra("Imena", Imena);
        startActivity(intent);
    }
}
