package com.example.karla.drinko;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

public class pravila_karla extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);//ZA BAR GORE
        setContentView(R.layout.activity_pravila_karla);

        globalni global = (globalni) getApplicationContext();

        if(global.getJezik()==3){
            TextView tekst=(TextView)findViewById(R.id.prav);
            tekst.setText("1.\tUnutar minigejma postoje tri tipa zadataka odnosno kartici.\n" +
                    "2.\tNa iduću karticu idemo pomoću pritiska na strelicu.\n" +
                    "3.\tKorisnik/i mora/ju napraviti određeni zadatak, ako ne naprave, moraju piti.\n" +
                    "4.\tDrugi tip je „virus“. Kod virusa je dano pravilo kojega se određeni korisnici moraju držati tako dugo dok ne dođe na red kartica koja obilježava kraj tog virusa. Ako se ne sjete držati pravila, ti korisnici moraju piti.\n" +
                    "5.\tTreći tip je „igra“. U tomu je dana igra koju korisnici igraju u krug tako dugo dok jedan korisnik ne pogriješi.\n" +
                    "6.\tMinigejm je gotov kada se izmijeni 20 kartica.\n" +
                    "7.\tKada netko mora dati guce drugim igračima, lako ih rasporedi na više osobi ili jednoj osobi da sve.\n" +
                    "8.\tNiti jedan korisnik nije prisiljen da mora piti na neki zadatak, ako to ne želi, ne mora.\n" +
                    "9.\tKorisnici lako odustanu ako ne žele dalje igrati ili kada im je dovoljno.\n");
            startAnimation();
        }
        else if(global.getJezik()==2){
            TextView tekst=(TextView)findViewById(R.id.prav);
            tekst.setText("1.\tZnotraj minigame obstajajo tri tipa besedila oz. kartici.\n" +
                    "2.\tNa naslednju karticu gremo z pritiskom na puščicu.\n" +
                    "3.\tPrvi tip je „Obična naloga“. Korisnik/i mora/ju narediti neko delo, ali pijeju če to ne narediju.\n" +
                    "4.\tDrugi tip je „Virus“. V temu je podano pravilo katerega se moraju držati korisnik/i dok ne pride kartica za konec virusa ali kartica za konec minigamea. Če se ne vpomniju na to, moraju piti.\n" +
                    "5.\tTretji tip je „Igra“. V temu je podana igra kateru koriniki špilaju v krug dok se en korisnik ne zmoti.\n" +
                    "6.\tMinigame je gotov kdaj se zmeni 20 kartici.\n" +
                    "7.\tKdaj negdo mora dati guce, lahko ih porazdelji na vejč osebi ali lahko da enoj osebi vse.\n" +
                    "8.\tNiti en korisnik neje prisiljen da mora piti na neko nalogo, če neče, potem ne mora.\n" +
                    "9.\tKorisniki lahko odustanejo kdaj kod očejo ali kdaj im je dovolj.\n");
            startAnimation();
        }
        else if(global.getJezik()==1){
            TextView tekst=(TextView)findViewById(R.id.prav);
            tekst.setText("1.\tInside of the minigame there are three types of tasks aka cards.\n" +
                    "2.\tFor next card, press the arrow.\n" +
                    "3.\tUser has to do the task or he/she has to drink if they refuse it.\n" +
                    "4.\tSecond type is „virus“. In this game, there is a rule that some user/users have to follow. They stop to follow the rule when the card for stopping comes. If they do not follow the rule, they have to drink.\n" +
                    "5.\tThird type is „game“. In this type, there's a game that specific or all players or users have to play. The „game“ finishes when one person makes mistake.\n" +
                    "6.\tMinigame is finished when 20 cards exchange.\n" +
                    "7.\tWhen someone has to give sips, they can give them all to one person or they can give less sips to more people.\n" +
                    "8.\tNobody is forced to drink on the task, if they don't want to, they don't have to.\n" +
                    "9.\tUsers or players can easily give up when they want or if it's enough for them.\n");
            TextView naslov=(TextView)findViewById(R.id.nasl);
            naslov.setText("RULES");
            startAnimation();
        }
        else{
            TextView tekst=(TextView)findViewById(R.id.prav);
            tekst.setText("Error!\n" + global.getJezik());
            TextView naslov=(TextView)findViewById(R.id.nasl);
            naslov.setText("RULES");
            startAnimation();
        }
    }

    public void startAnimation() {
        Animation anima = AnimationUtils.loadAnimation(this, R.anim.karla_prelaz_tekst);
        TextView txt_animation = (TextView) findViewById(R.id.prav);
        txt_animation.startAnimation(anima);

    }
}
