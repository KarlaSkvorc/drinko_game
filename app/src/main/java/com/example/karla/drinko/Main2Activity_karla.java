package com.example.karla.drinko;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Main2Activity_karla extends AppCompatActivity {

    private Button button;
    int index;
    int brojac=0;
    int virusi=0;
    int prije=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);//ZA BAR GORE
        setContentView(R.layout.activity_main2_karla);

        virusi=0;

        button = (Button) findViewById(R.id.button2_slo);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRules();
            }
        });
    }

    public void startAnimation() {
        Animation anima = AnimationUtils.loadAnimation(this, R.anim.karla_prelaz_tekst);
        TextView txt_animation = (TextView) findViewById(R.id.words_slo);
        txt_animation.startAnimation(anima);

    }

    public void openRules(){
        Intent intent = new Intent(this, pravila_karla.class);
        startActivity(intent);
    }

    public void generate_slo(View view) {//GENERIRANJE RANDOM BROJEVI
        brojac++;

        final ArrayList<String> arri = (ArrayList<String>) getIntent().getSerializableExtra("Imena");

        Random rand2 = new Random();
        int sz = rand2.nextInt(arri.size());
        String rand_name =arri.get(sz);

        if(brojac==20) {
            if(virusi==1){
                TextView words2 = (TextView) findViewById(R.id.words_slo);
                words2.setText("EPIDEMIJA JE KONČALA\n"+"ISTO KOT IN IGRA!");
                words2.setTextSize(50);

                TextView nasl = (TextView) findViewById(R.id.naziv_slo);
                nasl.setText(" ");


                ConstraintLayout lay = (ConstraintLayout) findViewById(R.id.layo2);
                lay.setBackgroundResource(R.drawable.nova_pozadina3);

                startAnimation();
            }
            else {
                TextView words = (TextView) findViewById(R.id.words_slo);
                words.setText("KONEC!");
                words.setTextSize(50);
                startAnimation();

                TextView nasl = (TextView) findViewById(R.id.naziv_slo);
                nasl.setText(" ");

                ConstraintLayout lay = (ConstraintLayout) findViewById(R.id.layo2);
                lay.setBackgroundResource(R.drawable.nova_pozadina2);
            }

        }
        else if(brojac>20){
            Intent intent = new Intent(this, plosca.class);
            final ArrayList<String> array = (ArrayList<String>) getIntent().getSerializableExtra("Imena");
            intent.putExtra("Imena", array);
            startActivity(intent);
        }
        else {
            Random rand = new Random();
            int number=rand.nextInt(132);
            if(prije==1){
                while(number >= 51 && number < 81) number = rand.nextInt(132);
            }

            if (number >= 0 && number < 51) {//obicne
                List<String> list;
                list = new ArrayList<String>();
                list.add(rand_name + ", trči po sobi kot kokoš ali pij 2-krat.");
                list.add("Če ste že kdaj poskusili uporabiti vse štiri barve na penkali, pijte 4-krat.");
                list.add("Dajte 2 guca, če še nikoli niste gledali nobenega od filmov o Gospodarju prstenov.");
                list.add("Vsi, ki so od začetka \"Drinkija\" bili na wcu, dajejo 3 guca.");
                list.add(rand_name + ", daješ toliko guci, kolikor imaš stricev.");
                list.add(rand_name + ", daš toliko guca, koliko imaš bratrancov.");
                list.add(rand_name + ", stoj na rokah ali pij 3-krat.");
                list.add(rand_name + ", naredi zvezdo ali pij 3-krat.");
                list.add(rand_name + ", naredi koluta naprej ali pij 2-krat.");
                list.add(rand_name + ", daj 4 guca ljudem višjim od tebe. Če to ni mogoče, potem pij 2-krat.");
                list.add("Oseba, ki ima mobilni telefon, mora določiti, kdo bo popil pijačo.");
                list.add(rand_name + ", označi en predmet v sobi. Kdor se ga najprej dotakne, daje 5 guc.");
                list.add(rand_name + ", izberi eno osebo v sobi. Kdor se ga najprej dotakne, daje 3 guca. Ta oseba nekomu daje en guc.");
                list.add(rand_name + ", pomešaj \"Vražjo pijačo\" desnome od sebe. Če ne želiš, pij 5-krat, če ta oseba ne želi piti, oba pijeta 10-krat.");
                list.add(rand_name + ", če si v vezi, daš 4 guca, če ne, daješ 2 guca.");
                list.add("Tisti, ki je nekomu lakiral nohte, daje 4 guca, a tisti, komu so lakirali, pa 2 guca.");
                list.add("Punce, popijte toliko, kot je vaša velikost nedrčka (A = 1, B = 2,…).");
                list.add("Fantje, pijete toliko, kolikor je velikost vaših gač(S = 1, M = 2,…).");
                list.add("Prvi, ki se spomene inspiracijskega stavka, daje tri guca.");
                list.add(rand_name + ", zapleši Gangnam Style ali pij 2-krat.");
                list.add(rand_name + ", zapleši Robot Dance ali pij 2-krat.");
                list.add(rand_name + ", zaplešite Macareno ali pijte 2-krat.");
                list.add("Igralci, ki niso glasovali na zadnjih volitvah, pijejo 4 guc.");
                list.add(rand_name + ", daš 4 guce igralcem, ki so tanjši od tebe. Če to ni mogoče, piješ dvakrat.");
                list.add(rand_name + ", daš 4 guce igralcem, ki so starješi od tebe. Če to ni mogoče, popij tiste 4 guce.");
                list.add(rand_name + ", daš 3 guce igralcem, ki so mlajši od tebe. Če to ni mogoče, popij 5 guc.");
                list.add(rand_name + ", če boš mirnem 5 minut, lahko izbereš, kdo bo pil tvojo pijačo.");
                list.add("Vsi, ki ste v tem tednu imeli seksi sanje, pijejo 3-krat.");
                list.add(rand_name + ", izberite, če želite 4 guca dati zadnjem igralcu, ki je bil na wcu, ali 1 guc vsem v sobi.");
                list.add("Vsi mlajši od " + rand_name + ", pijejo 3-krat.");
                list.add("Vsi starejši od " + rand_name + ", pijejo 3-krat.");
                list.add(rand_name + ", daj si toliko guca, kot nosiš bele stvari.");
                list.add("Vsi, ki ste hodili na fakulteto z " + rand_name +", popijte 2 guc.");
                list.add("Vsi, ki ste hodili v srednjo šolo z " + rand_name + ", pijejo 3 guc.");
                list.add(rand_name + ", če si kdaj rešil Rubikovo kocko, daš 4 guca. Če nisi, piješ te guce.");
                list.add(rand_name + ", naredi 5 potisk ali pij 5-krat.");
                list.add(rand_name + " naredi 10 potiskov ali pij 5-krat.");
                list.add("Pijte 1 guc, če ste svobodni, 2, če ste v vezi, 3, če imate FWB ali 4, če je zapleteno.");
                list.add(rand_name + ", zapri oči. Če lahko natančno poveš, kdo kaj pije, daš 5 guc. Če se zmotiš, piješ 2 krat.");
                list.add("Vsi pijejo tolikokrat, kot imajo leta od konca srednje šole.");
                list.add("Če studiraš, piješ 2-krat, če delaš, piješ 3-krat. Če nič drugega, pijete 5-krat.");
                list.add(rand_name + ", govori angleško abecedo od zadaj do začetka. Če se zmotiš, piješ 4-krat.");
                list.add(rand_name + ", če bi raje imel psa, daš 2 guca. Če mačko daš 1 guc.");
                list.add(rand_name + ", piješ toliko guc, kolikor je praznih kozarcev na mizi.");
                list.add("Vsi, ki imajo vozniško dovoljenje in ga aktivno uporabljajo, pijejo 1, ki nimajo vozniškega dovoljenja 3-krat in\n" +
                        "ki ga imajo in ne uporabljajo 5-krat!\n");
                list.add("Najnoviši voznik pije 3 krat!");
                list.add("Kdo ima najdaljše vozniško dovoljenje, daje 5 guc.");
                list.add(rand_name + ", pijte 1 guc pijače brez rok. Če ti uspe, daš 3 guc, če ne, daš ti pijačo.");
                list.add(rand_name + ", če si prišel peške ali z kolesima, pij 1 guc, če si prišel z javnim prevozom, piješ 3 guca.");
                list.add(rand_name + ", pokaži vsem zadnjih 5 slik v svoji galeriji. Če odbiješ, piješ 5-krat.");
                list.add(rand_name + ", odkleni mobitel desnom od sebe na eno minuto. Če odbiješ, moraš do konca spiti svojo pijačo.");


                Collections.shuffle(list, new Random(System.nanoTime()));
                TextView words2 = (TextView) findViewById(R.id.words_slo);
                words2.setText(list.get(index++));
                words2.setTextSize(20);


                TextView nasl = (TextView) findViewById(R.id.naziv_slo);
                nasl.setText("OBIČNA NALOGA");

                ConstraintLayout lay = (ConstraintLayout) findViewById(R.id.layo2);
                lay.setBackgroundResource(R.drawable.nova_pozadina1);
                prije=0;

                startAnimation();


            }
            else if (number >= 51 && number < 81) {//virus

                if(virusi==0) {
                    virusi++;
                    List<String> list2;
                    list2 = new ArrayList<String>();
                    list2.add(rand_name + ", obrni hrbet vsem v sobi. Povedali ti bomo, kdaj je čas, da se obrneš nazaj.");
                    list2.add(rand_name + ", pojdi v kotiček in pomisli, kaj si rekel. Povedali ti bomo, kdaj je čas za vrnitev.");
                    list2.add("Čas je za čopleka! Vsakič, ko kdo pije, si iz pijače vzemite namišljenega čopleka in ko končate, ga morate spraviti nazaj gor.");
                    list2.add("Vsak fant dvakrat pije, ko punčka pije.");
                    list2.add("Vsako dekle dvakrat pije, ko fant pije.");
                    list2.add("Čas je za gusarje! Vsak igralec mora na začetku in koncu stavka reči \"ARR\". Kdor gre narobe, pije 2 guc.");
                    list2.add("Čas je za \"Janeza\"! Vsakič morate na začetku in na koncu stavka reči \"ja ne\". Kdor gre narobe, pije 2 guc.");
                    list2.add("Vsakdo v kavbojkah mora spiti 2 guca vsakič, ko kdo pije.");
                    list2.add("Vsakdo v kratkih srajcah vsaki pot popije 2 guc.");
                    list2.add("Vsi s črnimi lasmi pijejo guc vsakič, ko nekatera punca pije.");
                    list2.add(rand_name + ", zamenjate sedeže drugim prijateljima. Vsakič, ko to storite, spijte 1 guc.");
                    list2.add("Čas je za slamico! Vsakič, ko kdo pije, vzemite namišljeno slamico iz svoje pijače, in ko končate, jo morate spraviti nazaj. Ne pozabite, da ga med pitjem ves čas držite v roki in ne pozabite ga pretresati, da preprečite razlitje.");
                    list2.add("Čas je za \"žalujočega\"! " + rand_name + ", ti si vlačilec, vsakič, ko se smejiš, moraš piti guc.");
                    list2.add("Prepovedano se je dotikati las in obraza! Kdor se zmoti, pije vsakič, ko se ga dotakne, štirikrat.");
                    list2.add("Brez kajenja! Kdor želi kaditi, mora piti 5-krat.");
                    list2.add("Ne glejte v oči! Kdor drug drugemu pogleda v oči, pije 3-krat.");
                    list2.add("Čas je za izziv Jen Selter! Vsakič, ko se kdo hoče odpraviti od kraja, mora narediti 10 počepov. Tisti, ki pozabi, pije 2-krat.");
                    list2.add("Čas je premora! Naj vsak predvajalnik vklopi zvok in internet na svojem mobilnem telefonu in ga postavi na sredino mize. Vsakič, ko je kdo obveščen, daje 3 guc.");
                    list2.add("Vsakič, kot nekaj nekomu padne, sosedje pijejo 5-krat.");
                    list2.add("Čas je za čajanko! Vsakič ko pije, mora piti z dvignjenim malim prstom. Kdor gre narobe, pije 3 guc.");
                    list2.add("Nihče se ne sme smejati. Kdo se smeji, pije 3 guc.");
                    list2.add(rand_name + ", vstani in zapleši! Če se ustavite, popijte pijačo!");
                    list2.add(rand_name + ", čarovnik si, abrakadabra! Zamenjajte neko pijačo, ko želite.");
                    list2.add(rand_name + " je glavni. Vsakič, ko boste ploskali z rokami, vsi pijejo (vključno z vami)!");
                    list2.add("Vsak igralec mora govoriti, kot da ima usta polna hrane. Kdor pozabi, pije pijačo 3-krat!");
                    list2.add(rand_name + ", vsakič, ko nekdo dokonča stavek, moraš reči \"očitno ...\". Če pozabite, popijte pijačo!");
                    list2.add(rand_name + ", postala si babica! Nikomur ne smeš pokazati zob. Če pokažeš, piješ 3-krat.\n");
                    list2.add(rand_name + ", postal služabnik osebe preko tebe. Izpolnite lahko do 5 njegovih želja.");
                    list2.add("Novo pravilo! Vsakič, ko nekdo govori, mora končati z \"" + rand_name + ", ti si mutec!\".");
                    list2.add("Čas je za kavboje in Indijance! Fantje so kavboji in morajo vsakič začeti stavek z \"Howdy!\", Dekleta pa so domače Amerike in morajo vsak stavek končati tako, da zbadajo usta in vpijejo \"oooo\".");


                    Collections.shuffle(list2, new Random(System.nanoTime()));
                    TextView words2 = (TextView) findViewById(R.id.words_slo);
                    words2.setText(list2.get(index++));
                    words2.setTextSize(20);
                    prije=1;


                    TextView nasl = (TextView) findViewById(R.id.naziv_slo);
                    nasl.setText("VIRUS");


                    ConstraintLayout lay = (ConstraintLayout) findViewById(R.id.layo2);
                    lay.setBackgroundResource(R.drawable.nova_pozadina3);

                    startAnimation();
                }

                else{
                    TextView words2 = (TextView) findViewById(R.id.words_slo);
                    words2.setText("EPIDEMIJA JE KONČALA!");
                    words2.setTextSize(50);

                    TextView nasl = (TextView) findViewById(R.id.naziv_slo);
                    nasl.setText(" ");


                    ConstraintLayout lay = (ConstraintLayout) findViewById(R.id.layo2);
                    lay.setBackgroundResource(R.drawable.nova_pozadina3);

                    startAnimation();
                    virusi=0;

                }


            }
            else{
                List<String> list3;
                list3 = new ArrayList<String>();
                list3.add("Govorite v krogu pribor, ki ga lahko vidite pri zobozdravniku. Kdo ne ve več, pije 3-krat. " + rand_name + ", začneš.");
                list3.add("Govorite v krogu, vsak ima 2 sekundi, da izgovori besedo, ki je povezana z besedo, ki jo je uporabnik izgovoril prej. " + rand_name + ", začneš. Tisti, ki ne ve več ali se zmoti, prije 3-krat.");
                list3.add("V krogu izgovorite: \"Na štriku se suši šareni šosič\", tisti, ki se zmoti, pije 3-krat. " + rand_name + ", začneš.");
                list3.add("V krogu govorite: \"Na vrh brda vrba mrda\", tisti, ki stori napako, pije 2-krat. " + rand_name + ", začneš.");
                list3.add("Govorite dodatno opremo za avtomehanika. Kdo ne ve več, pije 3-krat. " + rand_name + ", začneš.");
                list3.add("V krogu govorite predmete iz srednje šole, ki se jih je udeležil vsak uporabnik. " + rand_name + ", začneš. Kdor zmanjka idej, pije 2-krat.");
                list3.add("Govorite v krog Marvel junake. Kdor naredi prvo napako ali ne ve, pije 3-krat. "+ rand_name + ", začneš.");
                list3.add("Govorite v krog X-men junake. Kdor naredi prvo napako ali ne ve, dvakrat popije. "+ rand_name + ", začneš.");
                list3.add("Govorite v krog programske jezike. Kdor se zmoti ali ne ve, pije 4-krat. "+ rand_name + ", začneš.");
                list3.add("Govorite v krog o pošastima z bajki. Kdor naredi prvo napako ali ne ve, pije 2-krat. "+ rand_name + ", začneš.");
                list3.add("\"Za ali proti gela za lasje\"? Vsi glasujejo istočasno, poražena ekipa pije dvakrat.");
                list3.add("\"Za ali proti parfema na puncama\"? Vsi glasujejo istočasno, poražena ekipa pije dvakrat.");
                list3.add("\"Za ali proti seksa v javnosti\"? Vsi glasujejo istočasno, poražena ekipa pije dvakrat.");
                list3.add("\"Za ali proti tajci na fantima\"? Vsi glasujejo istočasno, poražena ekipa pije dvakrat.");
                list3.add("Za ali proti komunizma? Vsi glasujejo istočasno, poražena ekipa pije dvakrat.");
                list3.add("Govorite v krogu stvari z luknjami. Prvi, ki se je zmotil ali ne vem, pije 3-krat. "+ rand_name + ", začneš.");
                list3.add("\"Rum ali vodka\"? Vsi glasujejo istočasno, poražena ekipa pije dvakrat.");
                list3.add("\"Pivo ali vino\"? Vsi glasujejo istočasno, poražena ekipa pije dvakrat.");
                list3.add("\"belo ali rdeče vino\"? Vsi glasujejo istočasno, poražena ekipa pije dvakrat.");
                list3.add("\"Marvel ali DC\"? Vsi glasujejo istočasno, poražena ekipa pije dvakrat.");
                list3.add("\"Oditi v nebesa ali se reinkarnirati v boljše življenje\"? Vsi glasujejo istočasno, poražena ekipa pije dvakrat.");
                list3.add("Vsak igralec mora povedati nekaj, da ima " + rand_name + " več kot desni od tiste osebe. Oseba levo od "+ rand_name + "začne. Tisti, ki ne vedo ali napačno odgovorijo, pijejo 2-krat.");
                list3.add("Vsak igralec mora povedati nekaj, ki ga ima " + rand_name + " manj kot levi od tiste osebe. Oseba preko "+ rand_name + " začne. Kdor ne ve ali se zmoti, pije 3-krat.");
                list3.add("Vsak igralec mora povedati tisto, kar ga moti okoli " + rand_name + ". Prijatelj desno od "+ rand_name + " začne. Tisti, ki ne vedo ali napačno pijejo 2-krat.");
                list3.add("Lasje do kolena ali irokeje? Vsi glasujejo istočasno, poražena ekipa pije dvakrat.");
                list3.add("Govorite z državami okoli Slovenije. Kdor gre narobe ali ne zna, pije 2-krat. Na katerem se ustavi, pije 3-krat. "+ rand_name + ", začneš.");
                list3.add("Govorite z državami, ki obkrožajo Hrvaško. Kdor gre narobe ali ne zna pije 2-krat. Na katerem se ustavi, pije 3-krat. "+ rand_name + ", začneš.");
                list3.add("Govorite z državami okoli Združenega kraljestva. Kdor gre narobe ali ne zna piti 2-krat. Na katerem se ustavi, pije 3-krat. "+ rand_name + ", začneš.");
                list3.add("\"Države v Evropski uniji\"! " + rand_name + ", začneš. Kdor se zmoti ali ne ve, pije 3-krat. Na katerem se ustavi, pije 2-krat.");
                list3.add(rand_name + ", izgovori ime, priimek in datum rojstva vsakega igralca. Za katero osebo se motiš, ji da 2 guc.");
                list3.add("Čas je za \"meduze\"! Vsak igralec mora glavo počivati na robu mize. Na koncu odštevanja vsi pogledajo v oči ene osebe. Ljudje, ki si pogledajo v oči, izdihnejo svojo pijačo.");
                list3.add("Čas je za eno belo! Če imate s seboj mađarice, naj vsak igralec vzame enega. Igralec z najvišjo karto daje 4 guc, igralec z najnižjo pijačo 2 guca. Če nimate vstopnic, vsak igralec pije 5-krat.");
                list3.add("Čas je za eno belo! Če imate s seboj bele karte, naj vsak igralec vzame enega. Igralec z najvišjo karto popije 4 guc, igralec z najnižjo karto pa 2 guc. Če nimate vstopnic, vsak igralec pije 3-krat.");
                list3.add("Govorite v krogu o zadnjih filmih, ki ste jih gledali. Če si je nekdo drug ogledal isti film, oba pijeta 3-krat. "+ rand_name + ", začneš.");
                list3.add("V krogu se pogovorite o zadnji seriji, ki ste jo gledali. Če je kdo drug gledal isto serijo, oba pijeta 3-krat. "+ rand_name + ", začneš.");
                list3.add("Govorite o strateških video igrah. Kdor pove narobe ali ne zna, pije 2-krat. Na katerem se ustavi, pije 3-krat. "+ rand_name + ", začneš.");
                list3.add("Govorite igre za PS2. Kdor gre narobe ali ne zna pije 2-krat. Na katerem se ustavi, pije 3-krat.");
                list3.add("\"Psi ali mačke\"? Vsi glasujejo istočasno, poražena ekipa pije dvakrat.");
                list3.add("Čas je za »Končanje stavka«! Vsak igralec pove eno besedo, naslednji igralec pa mora ponoviti vse besede od prej in dodati svoje. Zapomni si pomen stavka! Kdo je naredil napako, 2-krat pije.");
                list3.add("Čas je slap! En igralec začne piti svojo pijačo, nato igralec za njim in tako v krogu. Ko prvi igralec preneha piti, to storite tudi vsi drugi igralci v krogu, eden za drugim. Igralec, ki predčasno začne ali ustavi, popije še 3 guca. Če je slap uspel, prvi popije 3 guca. " + rand_name + ", začni znova.");
                list3.add("Čas je za \"kategorijo\"! " + rand_name + ", pripravi kategorijo in vsi drugi igralci morajo šteti, dokler se kdo ne zmoti ali ve. Ta pije 3 guca.");
                list3.add("Čas je za higieno! Vsi si umijejo roke, kdor najprej opere, da 2 guc, ki zadnji, pije 2 guc.");
                list3.add("Govorite v krog najljubše jedi od " + rand_name + ". Kdor ne ve več ali dela narobe, pije 3-krat. Prijatelj levo od "+ rand_name + ", začne.");
                list3.add(rand_name + ", rečeš \"V mojem kovčku je ..\" in rečeš eno stvar. Vsak igralec mora ponoviti vse predmete in reči nekaterega svojega predmeta. Ki se zmoti, pije 3 krat.");
                list3.add("\"Pričeske iz 80-ih ali oblačila 80-ih\"? Vsi glasujejo istočasno, poražena ekipa pije dvakrat.");
                list3.add("Čas je za pesem. " + rand_name + ", začni pesem in pokaži na igralca, ki bo nadaljeval! To traja, dokler nekdo ne stori napake ali ne pozna besed. Vsi, ki so peli, pijejo 2 guc.");
                list3.add("\"rentgenski ali laserski pogled\"? Vsi glasujejo istočasno, poražena ekipa pije dvakrat.");
                list3.add("Govorite z znanim kriminalcem. Kdor naredi prvo napako ali ne ve, pije 2-krat. "+ rand_name + ", začneš.");
                list3.add("Odločite se, kdo je najbolj pametna oseba v sobi. Ta oseba mora piti 3-krat.");
                list3.add("Odločite se, kdo je najlepša oseba v sobi. Ta oseba mora piti 3-krat.");
                list3.add("Čas je za eno \"BUM\"! Štetje od 1 do vsake številke, ki vsebuje 3 ali je deljivo s 3, morate reči \"bum\". Kdor gre narobe, pije 3 guc.");


                Collections.shuffle(list3, new Random(System.nanoTime()));
                TextView words2 = (TextView) findViewById(R.id.words_slo);
                words2.setText(list3.get(index++));
                words2.setTextSize(20);
                prije=0;

                TextView nasl = (TextView) findViewById(R.id.naziv_slo);
                nasl.setText("IGRA");


                ConstraintLayout lay = (ConstraintLayout) findViewById(R.id.layo2);
                lay.setBackgroundResource(R.drawable.nova_pozadina2);

                startAnimation();
            }
        }
    }
}
